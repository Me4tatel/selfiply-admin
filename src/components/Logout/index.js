import React, { Component } from 'react';
import withRouter from "react-router/es/withRouter";
import { connect } from 'react-redux';
import { logout } from '../../view/js/actions/login.js';

class Logout extends Component{
  constructor(props){
    super(props);
    this.props.onLogout();
      if(!this.props.logined){
          this.props.history.push('/login');
      }
  }
  componentWillReceiveProps(nextProps){
    if(this.props.logined){
      this.props.history.push('/login');
    }
  }

  render() {
    return (
      <div>
        <h1>Loading...</h1>
      </div>
    );
  }
}

export default withRouter(connect(
    (state, ownProps) => ({
        logined: state.login,
		    ownProps
    }),
    dispatch => ({
        onLogout: (data) => {
		         dispatch(logout());
        },
    })
)(Logout));
