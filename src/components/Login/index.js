import React, { Component } from 'react';
import withRouter from "react-router/es/withRouter";
import { connect } from 'react-redux';
import { login } from '../../view/js/actions/login.js';

import logo from '../../view/img/logo.svg';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email : '',
            password: ''
        };
        if(this.props.logined){
            this.props.history.push('/');
        }
    }

    handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
    }

    componentWillReceiveProps(nextProps){
        console.log('here');
        if(nextProps.logined){
            this.props.history.push('/');
        }
    }

    onSubmit = (event) => {
        event.preventDefault();
        console.log('submit');
        this.props.onLogin(this.state);
    }

    render() {
        return (
            <div className="fullscreen center">
                <div className='login-form'>
                    <form onSubmit={this.onSubmit}>
                        <div className="by-top">
                            <img src={logo} alt=""/>
                        </div>
                        <div className="content">
                            <div className="field">
                                <label>
                                    <span>Login</span>
                                    <input
                                        placeholder='E-mail address'
                                        type="email"
                                        name="email"
                                        value={this.state.email}
                                        onChange={this.handleInputChange}
                                        required
                                    />
                                </label>
                            </div>
                            <div className="field">
                                <label>
                                    <span>Password</span>
                                    <input
                                        placeholder='Password'
                                        value={this.state.password}
                                        onChange={this.handleInputChange}
                                        type='password'
                                        name="password"
                                        required
                                    />
                                </label>
                            </div>
                            <div className="field">
                                <button className="btn submit-btn">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(connect(
    (state, ownProps) => ({
        logined: state.login,
        ownProps
    }),
    dispatch => ({
        onLogin: (data) => {
            dispatch(login(data));
        },
    })
)(Login));
