import React, {Component} from "react";
import { sortable } from 'react-sortable';

class Item extends Component {
    render() {
        let { item, isDeleted } = this.props;
        return (
            <li {...this.props}
                className={"list-item" + (isDeleted?' delete': '')}
                key={item.id}>
                <div className="left">
                    <div className="icon-move">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.25 21">
                            <circle className="cls-1" cx="9.87" cy="6.74" r="1.87"/>
                            <circle className="cls-1" cx="9.87" cy="14.38" r="1.87"/>
                            <circle className="cls-1" cx="2.37" cy="2.37" r="1.87"/>
                            <circle className="cls-1" cx="2.37" cy="10.5" r="1.87"/>
                            <circle className="cls-1" cx="2.37" cy="18.63" r="1.87"/>
                        </svg>
                    </div>
                    <span>{item.text}</span>
                </div>
                <div className="right">
                    <span className="controll-btn" onClick={() => {
                        this.props.removeConcernSymptom()
                    }}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.03 19.88">
                            <line className="cls-1" x1="0.5" y1="19.38" x2="19.38" y2="0.5"/>
                            <line className="cls-1" x1="19.53" y1="19.38" x2="0.65" y2="0.5"/>
                        </svg>
                    </span>
                    <div className="back-action">This element was disconnected. <div className="make" onClick={()=>(this.props.cancelRemove())}>Undo</div></div>
                </div>
            </li>
        )
    };
}
let SortableItem = sortable(Item);

export default SortableItem;