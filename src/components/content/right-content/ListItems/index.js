import React, { Component } from 'react';
import withRouter from "react-router/es/withRouter";
import { connect } from 'react-redux';
import { changeOrderConcern } from "../../../../view/js/actions/concerns";
import SortableItems from "./SortableItems"

class ListItems extends Component{
    constructor(props){
        super(props);
        this.changeOrder = this.changeOrder.bind(this);
    }
    changeOrder(orders){
        this.props.page === "concerns" && this.props.changeOrderConcern(orders)
    }
	render(){
        let { items, onChoose:checkSiblings, openModal, removeItem, cancelRemove, listRemove, page, choosedItem } = this.props;
		return (
            <SortableItems
                items={items}
                changeOrder={this.changeOrder}
                checkSiblings={checkSiblings}
                openModal={openModal}
                removeItem={removeItem}
                cancelRemove={cancelRemove}
                listRemove={listRemove}
                choosedItem={choosedItem}
                page={page}
            />
        );
	}
}

export default withRouter(connect(
    (state, ownProps) => ({
        store: state,
        concerns: state.concerns,
		ownProps
    }),
    dispatch => ({
        changeOrderConcern: (list) => {
            dispatch(changeOrderConcern(list));
        }
    })
)(ListItems));