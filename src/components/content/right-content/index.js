import React, {Component} from 'react';
import withRouter from "react-router/es/withRouter";
import CustomScroll from "react-custom-scroll";
import {connect} from 'react-redux';

import {getConcerns, getConcernSymptoms, deleteConcern} from '../../../view/js/actions/concerns.js';
import {getSymptoms, getSymptomConcerns, deleteSymptom} from '../../../view/js/actions/symptoms.js';

import ListItems from './ListItems';
import BlockSiblings from './BlockSiblings';

class RightContent extends Component {
    constructor(props) {
        super(props);
        this.props.onGetConcerns();
        this.props.onGetSymtoms();
        this.state = {
            chooseSiblings: false,
            choosedId: '',
            subItems: [],
            forRemove: {},
        };
        this.checkSiblings = this.checkSiblings.bind(this);
        this.checkModal = this.checkModal.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.tryRemove = this.tryRemove.bind(this);
        this.cancelRemove = this.cancelRemove.bind(this);
    }

    checkModal(name, id) {
        let {openModal, closeModal, stateModal} = this.props;
        if (stateModal) {
            closeModal();
        } else {
            if (this.props.match.params.page === "concerns") {
                if (name && id) {
                    openModal({
                        text: 'Change concern',
                        do: 'changeConcern',
                        id: id,
                        elements: [
                            {
                                type: 'input',
                                title: 'title',
                                require: true,
                                placeholder: 'Input name',
                                value: name,
                                name: 'name'
                            }
                        ]
                    });
                } else {
                    openModal({
                        text: 'Create a concern',
                        do: 'createConcern',
                        elements: [
                            {
                                type: 'input',
                                title: 'title',
                                require: true,
                                placeholder: 'Input name',
                                name: 'name'
                            }
                        ]
                    });
                }
            } else {
                console.log(name, id);
                if (name && id) {
                    let elem = this.props.symptoms.find(symptom => symptom.id === id);
                    openModal({
                        text: 'Change symptom',
                        do: 'changeSymptom',
                        id: id,
                        elements: [
                            {
                                type: 'input',
                                title: 'title',
                                require: true,
                                placeholder: 'Input name',
                                name: 'name',
                                value:name
                            },
                            {
                                type: 'select',
                                title: 'type',
                                require: true,
                                placeholder: 'Pick the option',
                                name: 'inputType',
                                value:elem.inputType
                            },
                            {
                                type: 'input',
                                title: 'Placeholder',
                                require: false,
                                placeholder: 'This will be displayed in the input ﬁeld for the patient',
                                name: 'placeHolder',
                                value:elem.placeHolder
                            }
                        ]
                    })
                } else {
                    openModal({
                        text: 'Create a symptom',
                        do: 'createSymptom',
                        elements: [
                            {
                                type: 'input',
                                title: 'title',
                                require: true,
                                placeholder: 'Input name',
                                name: 'name'
                            },
                            {
                                type: 'select',
                                title: 'type',
                                require: true,
                                placeholder: 'Pick the option',
                                name: 'inputType'
                            },
                            {
                                type: 'input',
                                title: 'Placeholder',
                                require: false,
                                placeholder: 'This will be displayed in the inputﬁeld for the patient',
                                name: 'placeHolder'
                            }
                        ]
                    })
                }

            }
        }

    }

    componentWillReceiveProps(nextProps) {
        if (this.state.choosedId) {
            if (this.props.match.params.page === "concerns") {
                this.setState({
                    subItems: nextProps.concerns.find(x => x.id === this.state.choosedId).symptoms
                })
            } else {
                // this.setState({
                //     subItems: nextProps.symptoms.find(x => x.id === this.state.choosedId).concerns
                // })
            }
        }
    }

    checkSiblings(id) {
        if (this.state.choosedId !== id) {
            if (this.props.match.params.page === "concerns") {
                this.props.onGetConcernSymptoms(id);
            } else {
                this.props.onGetSymtomConcerns(id);
            }
            this.setState({
                choosedId: id
            });
            this.setState({
                chooseSiblings: true
            });
        } else {
            this.setState({
                chooseSiblings: false
            });
            this.setState({
                choosedId: ''
            });
        }
    }

    closeBlockChooseSiblings() {
        this.setState({
            chooseSiblings: false,
            choosedId: ''
        });
    }

    removeItem(id){
        if(id === this.state.choosedId){
            this.setState({
                choosedId: '',
                chooseSiblings: false,
            });
        }
        this.props.match.params.page === "concerns" ? this.props.removeConcern(id) : this.props.removeSymptom(id);
    }

    tryRemove(id){
        let { forRemove } = this.state;
        let newForRemove = {...forRemove};
        newForRemove[id] = setTimeout(()=>{
            this.removeItem(id);
        }, 10000);
        this.setState({
            forRemove: newForRemove,
        })
    }

    cancelRemove(id){
        let { forRemove } = this.state;
        let newForRemove = {...forRemove};
        clearTimeout(newForRemove[id]);
        delete newForRemove[id];
        this.setState({
            forRemove: newForRemove
        })
    }

    render() {
        let items = this.props.match.params.page === "concerns" ? this.props.concerns : this.props.symptoms;
        let { subItems, forRemove } = this.state;
        let subItemsIds = subItems.map(item => item.id);
        let pageName = this.props.match.params.page;
        return (
            <div className="right-content">
                <div className="top-menu d-flex f-between f-align-center">
                    <h2>{pageName[0].toUpperCase() + pageName.substring(1)}</h2>
                    <div className="btn" onClick={this.checkModal}>Create new {pageName.substring(0, pageName.length-1)}</div>
                </div>
                <div className={`d-flex content-right-sidebar ${this.state.chooseSiblings && this.props.match.params.page === "concerns" ? 'choose-sibling' : ''}`}>
                    <CustomScroll heightRelativeToParent="100%">
                        <ListItems page={this.props.match.params.page} items={items} choosedItem={this.state.choosedId} removeItem={this.tryRemove}
                                   onChoose={this.checkSiblings} onClose={this.closeBlockChooseSiblings}
                                   openModal={this.checkModal} listRemove={forRemove} cancelRemove={this.cancelRemove}/>
                    </CustomScroll>
                    <BlockSiblings items={subItems} itemsId={subItemsIds} parentId={this.state.choosedId} onRemove={this.removeSubitem}
                                   page={this.props.match.params.page}/>
                </div>
            </div>
        );
    }
}

export default withRouter(connect(
    (state, ownProps) => ({
        store: state,
        concerns: state.concerns,
        symptoms: state.symptoms,
        ownProps
    }),
    dispatch => ({
        onGetSymtoms: () => {
            dispatch(getSymptoms());
        },
        onGetSymtomConcerns: (id) => {
            dispatch(getSymptomConcerns(id));
        },
        onGetConcerns: () => {
            dispatch(getConcerns());
        },
        onGetConcernSymptoms: (id) => {
            dispatch(getConcernSymptoms(id));
        },
        removeConcern: (id) => {
            dispatch(deleteConcern(id));
        },
        removeSymptom: (id) => {
            dispatch(deleteSymptom(id));
        },
    })
)(RightContent));