import React, { Component } from 'react';
import { Link } from "react-router-dom";
import withRouter from "react-router/es/withRouter";
import { connect } from 'react-redux';

class LeftSidebar extends Component{	
	render(){
		return (
            <div className="left-sidebar">
                <Link to="/" className="logo">
                    <img src={require("../../../view/img/logo.svg")} alt="" />
                </Link>
                <ul>
                    <li className={this.props.match.params.page==="concerns"?'active':''}>
                        <Link to="/concerns">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.02 22.02">
                                <circle className="cls-1" cx="11.01" cy="11.01" r="10.51" />
                                <line className="cls-1" x1="11.01" y1="12.76" x2="11.01" y2="5.88" />
                                <line className="cls-1" x1="11.01" y1="16.33" x2="11.01" y2="15.85" />
                            </svg>
                            <span>Concerns</span>
                        </Link>
                    </li>
                    <li className={this.props.match.params.page==="symptoms"?'active':''}>
                        <Link to="/symptoms">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.07 21">
                                <path className="cls-1" d="M15.37.5h1.2V4.69a4.74,4.74,0,0,1-4.74,4.74h0A4.74,4.74,0,0,1,7.1,4.69V.5H8.3" />
                                <path className="cls-1" d="M11.83,16.11a4.39,4.39,0,0,1-8.78,0" />
                                <line className="cls-1" x1="11.83" y1="16.11" x2="11.83" y2="9.43" />
                                <circle className="cls-1" cx="3.08" cy="13.53" r="2.58" />
                            </svg>
                            <span>Symptoms</span>
                        </Link>
                    </li>
                    <li>
                        <Link to="/logout">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.2 21">
                                <polyline className="cls-1" points="9.85 20.5 0.5 20.5 0.5 0.5 9.85 0.5" />
                                <line className="cls-1" x1="18.7" y1="10.5" x2="5.17" y2="10.5" />
                                <polyline className="cls-1" points="14.77 6.57 18.7 10.5 14.77 14.43" />
                            </svg>
                            <span>Log Out</span>
                        </Link>
                    </li>
                </ul>
            </div>			
        );
	}
}

export default withRouter(connect(
    (state, ownProps) => {
        return{
            ownProps
        }
    }
)(LeftSidebar));