import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';

import withAuth from '../withAuth/';
import Content from '../content/';
import Login from '../Login/';
import Logout from '../Logout/';

import { connect } from 'react-redux';
import Redirect from "react-router/es/Redirect";


class app extends Component{
	render(){
        let redirectLink = this.props.logined ? '/concerns' : '/login';
		return (
			<div>
				<Switch>
                    <Route exact path="/login" render={(match) => {
                        return (
                            <Login match={match} history={this.props.history} location={this.props.location}/>
                        )
                    }
                    }/>
                    <Route exact path="/logout" render={(match) => {
                        return (
                            <Logout match={match} history={this.props.history} location={this.props.location}/>
                        )
                    }
                    }/>
					<Route exact path="/:page" component={withAuth(Content)}/>
                    <Route component={() => <Redirect to={redirectLink}/>}/>
				</Switch>
			</div>	
		);
	}

}

export default withRouter(connect(
	(state, ownProps) => ({
        logined: state.login,
		ownProps
	}))(app)
);