import * as axios from 'axios';

let dataTrasporter = axios.create({
  baseURL: '/api',
});

export default dataTrasporter;