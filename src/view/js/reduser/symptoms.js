const DOWLOAD_SYMPTOMS = "DOWLOAD_SYMPTOMS";
const ADD_SYMPTOM = "ADD_SYMPTOM";
const REMOVE_SYMPTOM = "REMOVE_SYMPTOM";
const CHANGE_SYMPTOM = "CHANGE_SYMPTOM";
const REMOVE_SYMPTOM_CONCERN = "REMOVE_SYMPTOM_CONCERN";

export default function (state = [], action){
	switch (action.type) {
		case ADD_SYMPTOM:{
			return state.concat( action.payload );
		}

        case REMOVE_SYMPTOM:{
            return state.filter((item) => item.id !== action.payload.id);
        }

		case DOWLOAD_SYMPTOMS:{			
			return [].concat( action.payload );
		}

        case CHANGE_SYMPTOM:{
            let newObj = state.map((item)=>{
                if(item.id === action.payload.id){
                    return{
                        ...item,
                        text: action.payload.text,
                        inputType: action.payload.inputType,
                        placeHolder: action.payload.placeHolder
                    };
                }
                return item;
            });
            return newObj;
        }

		case REMOVE_SYMPTOM_CONCERN:{
			return state.filter((item) => item.id !== action.payload.id);
		}

		default:
			return state
	}
}