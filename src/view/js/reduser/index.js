import { combineReducers } from 'redux';
import Symptoms from './symptoms.js';
import Concerns from './concerns.js';
import login from './login.js';
import { routerReducer } from 'react-router-redux';
import search from './search.js';

const  allReducers = combineReducers ({
	symptoms: Symptoms,
	concerns: Concerns,
    login: login,
	routing: routerReducer,
	search: search, 
});

export default allReducers