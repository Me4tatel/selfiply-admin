const dataTrasporter = require('../core/data-trasporter.js').default;

export const login = (data) => {
    let headers = {
        'Content-Type': 'application/json'
    }
    return dataTrasporter.post('/authenticate', data,  { headers:headers })
        .then(res => {
            if (res.status === 200) {
                return {
                    type: 'LOGIN',
                };
            } else {
                const error = new Error(res.error);
                throw error;
                return {
                    type: 'LOGOUT',
                };
            }
        })
        .catch(err => {
            console.error(err);
            alert('Error logging in please try again');
            return {
                type: 'LOGOUT',
            };
        });
}

export const logout = () => {
    return dataTrasporter.get('/logout')
        .then(res => {
            if (res.status === 200) {
                return {
                    type: 'LOGOUT',
                };
            } else {
                return {
                    type: 'LOGIN',
                };
            }
        })
        .catch(err => {
            console.error(err);
            return {
                type: 'LOGIN',
            };
        });

}

export const checkLogin = () => {
    return dataTrasporter.get(`/checkToken`).then(res => {
        if (res.status === 200) {
            return {
                type: 'LOGIN',
            };
        } else {
            return {
                type: 'LOGOUT',
            };
            const error = new Error(res.error);
            throw error;
        }
    })
        .catch(err => {
            console.error(err);
            return {
                type: 'LOGOUT',
            };
        });
}
