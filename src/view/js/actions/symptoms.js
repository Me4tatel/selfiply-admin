const dataTrasporter = require('../core/data-trasporter.js').default;

export function getSymptoms(){
	return dataTrasporter.get('Symptoms')
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'DOWLOAD_SYMPTOMS',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const addSymptom = (data) =>{
	return dataTrasporter.post(`Symptoms?name=${data.name}&inputType=${data.inputType}&placeHolder=${data.placeHolder}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'ADD_SYMPTOM',
				payload: result.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const changeSymptom = (data) =>{
	return dataTrasporter.put(`Symptoms/${data.id}?name=${data.name}&inputType=${data.inputType}` +
		(data.placeHolder?`&placeHolder=${data.placeHolder}`:''))
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'CHANGE_SYMPTOM',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const deleteSymptom = (id) =>{
	return dataTrasporter.delete(`Symptoms/${id}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'REMOVE_SYMPTOM',
				payload: {id}
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const getSymptomConcerns = (id) =>{
	return dataTrasporter.get(`SymptomConcerns/${id}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'DOWLOAD_SYMPTOM_CONCERNS',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const removeSymptomConcern = (id) =>{
	return dataTrasporter.delete(`SymptomConcern/${id}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in removing objects");
			}
			return {
				type: 'REMOVE_SYMPTOM_CONCERN',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}