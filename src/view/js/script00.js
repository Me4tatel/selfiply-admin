jQuery(document).ready(function($) {

	$('.mobile_button').click(function (){
		$('.mobile_button, .left_menu').toggleClass('active');
		$('.preset_panel.review').removeClass('review');
		$('.preset_panel .offer.active').removeClass('active');
	});

	$(window).resize(function (){
		$('.mobile_button.active').click();
		$('.gender_list.open').removeClass('open');
	});


	$('.gender_button').click(function (){
		$('.gender_list').toggleClass('open');
	});

	$(document).on('click', '.gender_list li', function (){
		$(this).closest('.gender_list').find('li.active').removeClass('active');
		$(this).addClass('active');
		var data_id = $(this).attr('data-id');
		$('.gender_button').removeClass().addClass('gender_button').addClass(data_id);
		$('.gender_list').removeClass('open');
	});

	$(function (){
		var data_id = $('.gender_list').find('li.active').attr('data-id');
		$('.gender_button').addClass(data_id);
	});

	$("#range_age").ionRangeSlider({
		hide_min_max: true,
		keyboard: true,
		min: 18,
		max: 55,
		from: 8,
		to: 55,
		type: 'double',
		step: 1,
		prefix: "",
		grid: false
	});



	$(document).on('click', 'header, .right_block', function (){
		$('nav').find('li.active').removeClass('active active_list');
	});

	$(document).on('click', '.right_block', function (){
		$('.gender_list.open').removeClass('open');
	});

	$(document).on('mouseleave', '.left_menu', function (){
		$('li.list.active').removeClass('active');
	});



	//choice women
	$(document).on('click', '.choice_panel .offer', function (){
		var w = $(window).outerWidth();
		if(w <= 992) {
			$('.mobile_button.active').click();
		}
		var a = $(this);
		if(a.hasClass('active')) {
			$(this).closest('.choice_panel').find('.offer.active').removeClass('active');
			$('.preset_panel').removeClass('review');
			$('.slider .present img').removeClass('active');
		}
		else {
			$(this).closest('.choice_panel').find('.offer.active').removeClass('active');
			$(this).addClass('active');
			review_info();
			$('.preset_panel').addClass('review');
			$('.slider .present img').removeClass('active');
			var b = a.find('img').clone().addClass('active');
			$('.slider .present').prepend(b);
			$('.preset_panel .details').animate({
				scrollTop: 0
			}, 900);
			key_button();


		}
	});


	//slider
	$(document).on('click', '.arrow.next', function (){
		var next_clide = $('.present ').find('img.active').removeClass().next('img').addClass('active');
		if(!next_clide.length) {
			$('.present ').find('img.active').removeClass('active');
			$('.present img').first().addClass('active');
		}
	});

	$(document).on('click', '.arrow.prev', function (){
		var prev_clide = $('.present ').find('img.active').removeClass().prev('img').addClass('active');
		if(!prev_clide.length) {
			$('.present ').find('img.active').removeClass('active');
			$('.present img').last().addClass('active');
		}
	});

	$(document).on('click', '.close', function (){
		$('.preset_panel').removeClass('review');
		$('.preset_panel').find('.offer.active').removeClass('active');
	});

	key_button();

	// FILTER

	$(document).on('click', 'li.list >span', function (){
		var a = $(this).closest('li');
		if (a.hasClass('active')) {
			$(this).parents('ul').find('li.active').removeClass('active ');
		}
		else {
			$(this).parents('ul').find('li.active').removeClass('active ');
			$(this).closest('li').addClass('active');
		}
		
	});

	$(document).on('click', '.list_choice li', function (){
		var a = $(this);
		var name = a.text();
		var id_list = $(this).attr('data-id-list');
		var group = $(this).parents('.list').attr('data-id');

		if(a.hasClass('checked')) {
			$(this).removeClass('checked');
			var cut = $('.result_choise_list_filter').find('.parameter[data-id-list='+id_list+']').addClass('close');
			setTimeout(function (){
				cut.remove();
				parameter_length();
				has_checked();
			},300);
			
		}
		else {
			$(this).addClass('checked');
			has_checked();
			wrapper();
			var past = $('.result_choise_list_filter').find('.'+group).append(
				'<div class="parameter" data-id-list="'+id_list+'">'+
				'<span class="name">'+name+'</span>'+
				'<span class="remove"></span>'+
				'</div>'
				);
			setTimeout(function (){
				past.find('.parameter').addClass('view');
			},10);
		}

	});

	// remove parameter 
	$(document).on('click', '.parameter .remove', function (){
		var a = $(this).closest('.parameter');
		var data_id_list = a.attr('data-id-list');
		
		// a.addClass('close');
		a.removeClass('view');


		setTimeout(function (){
			// a.closest('.parameter').remove();
			parameter_animate();
			// parameter_length();
			$('.filters').find('li[data-id-list='+data_id_list+']').removeClass('checked');
			has_checked();
		},500);

		setTimeout(function (){
			a.closest('.parameter').remove();
			parameter_animate();
			// parameter_length();
			// $('.filters').find('li[data-id-list='+data_id_list+']').removeClass('checked');
			// has_checked();
		},550);
	});

	// $(document).on('click', '.parameter', function (){
	// 	$(this).fadeToggle(100)
	// });



	id_filter_list();
	rating();
});

function id_filter_list(){
	var i = 1;
	$('.filters li.list').each(function() {
		$(this).attr('data-id','list'+i++);
	});
	id_filter_sublist();
}

function id_filter_sublist(){
	$('.filters .list').each(function() {
		var a = $(this).attr('data-id');
		var b = $(this).find('.list_choice');
		var b_lenght =b.children('li').length ;
		var i = 1;
		b.children('li').each(function() {
			$(this).attr('data-id-list',a+'_'+i++);
		});

	});
}

function key_button(){
	if($('.preset_panel').hasClass('review') || $('section').hasClass('banner')){
		$('html').on('keydown', function (events){
			if (events.keyCode == 39) {
				$('.arrow.next').click();
			} else if (events.keyCode == 37) {
				$('.arrow.prev').click();
			} else if (events.keyCode == 27) {
				$('.close').click()
			}
		});

	}
}

function has_checked(){
	
	$('.filters li.list').each(function() {
		var a = $(this).find('li.checked').length;
		if(a > 0) {
			$(this).addClass('has_checked');
		}
		else {
			$(this).removeClass('has_checked');
		}
	});
	
}
;
function wrapper(){
	var data_id = $('.filters ').find('.active').attr('data-id');
	var search = $('.result_choise_list_filter').find('.'+data_id);
	if(search.length == 0) {
		var id_name = $('.filters li.active').find('em.name').text();
		var wrap_group = '<div class="wrap_parametrs '+data_id+'">'+
		'<span class="name_group_parametrs">'+id_name+'</span>'+
		'</div>';
		$('.result_choise_list_filter').prepend(wrap_group);
	}	
}

function parameter_length(){
	$('.result_choise_list_filter').find('.wrap_parametrs').each(function() {
		var a = $(this);
		var b = a.find('.parameter').length;
		if(b == 0) {
			$(this).addClass('close');
			a.remove();
		}
	});
}

function parameter_animate(){
	$('.result_choise_list_filter').find('.wrap_parametrs').each(function() {
		var a = $(this);
		var b = a.find('.parameter').length;
		if(b == 0) {
			a.addClass('close');
			setTimeout(function (){
				a.remove();
			},700);
		}
	});
}
function review_info(){
	// from
	var offer = $('.choice_panel .offer.active');
	var name = offer.find('.nickname').text();
	var age = offer.find('.age').text();
	var link = offer.find('.href').text();
	var title = offer.find('.title').text();
	var rating = offer.find('.rating').text();
	var more_details = offer.find('.more_details').clone();
	var other = offer.find('.other').clone();
	
	//effect
	//to

	if(!$('.preset_panel').hasClass('review')) {
		$('.details').find('.nikname').text(name);
		$('.details').find('.cost').text(age);
		$('.details').find('.btn_review').attr('href',link);
		$('.details').find('.top_description').text(title);
		$('.details .rating li.active').removeClass('active');
		for(var i = 1; i <= rating; i++) {
			$('.details .rating li:nth-child('+i+')').addClass('active');
		}

		$('.details .more_details, .details .other').remove();
		$('.details .head').after(more_details);
		$('.details .more_details').after(other);
	}

	else {
		$('.rating, .more_details, .other, .nikname, .cost, .top_description').addClass('hidden');
		setTimeout(function (){
			$('.details').find('.nikname').text(name);
			$('.details').find('.cost').text(age);
			$('.details').find('.btn_review').attr('href',link);
			$('.details').find('.top_description').text(title);
			$('.details .rating li.active').removeClass('active');
			for(var i = 1; i <= rating; i++) {
				$('.details .rating li:nth-child('+i+')').addClass('active');
			}

			$('.details .more_details, .details .other').remove();
			$('.details .head').after(more_details);
			$('.details .more_details').after(other);
		},430);
		setTimeout(function (){
			$('.rating, .more_details, .other, .nikname, .cost, .top_description').removeClass('hidden');
		},500)
	}
	
	
	
}

function rating(){
	
}
